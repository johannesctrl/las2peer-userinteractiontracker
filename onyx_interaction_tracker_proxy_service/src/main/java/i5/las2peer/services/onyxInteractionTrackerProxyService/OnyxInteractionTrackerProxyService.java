package i5.las2peer.services.onyxInteractionTrackerProxyService;

import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.util.logging.Level;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import i5.las2peer.api.Context;
import i5.las2peer.api.logging.MonitoringEvent;
import i5.las2peer.logging.L2pLogger;
import i5.las2peer.restMapper.RESTService;
import i5.las2peer.restMapper.annotations.ServicePath;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * The las2peer ONYX Interaction Tracker is a service to facilitate communication between the ONYX Interaction Tracker
 * and a designated Learning Record Store. The ONYX Interaction Tracker is a JavaScript-Script that can run in ONYX to track
 * inputs a user does while working on a task. On every change of an input the JavaScript-Script sends a POST-Method to
 * this service.
 */
@Api
@SwaggerDefinition(
		info = @Info(
				title = "ONYX Interaction Tracker",
				version = "1.0.0",
				description = "A las2peer service that allows the gathering, processing and forwarding data to an LRS.",
				contact = @Contact(
						name = "Johannes Albrecht",
						email = "johannesalbrechtmail@gmail.com")
		)
)
@ServicePath("/las2peer-onyx-interaction-tracker")
public class OnyxInteractionTrackerProxyService extends RESTService {

	/**
	 * Gives a response to the onyx interaction tracker for it to check if the connection is still connected.
	 *
	 * @return Returns an HTTP response with a string as content.
	 */
	@GET
	@Path("/get-check-connection")
	@Produces(MediaType.TEXT_PLAIN)
	@ApiOperation(
			value = "REPLACE THIS WITH AN APPROPRIATE FUNCTION NAME",
			notes = "REPLACE THIS WITH YOUR NOTES TO THE FUNCTION")
	@ApiResponses(
			value = {@ApiResponse(
					code = HttpURLConnection.HTTP_OK,
					message = "REPLACE THIS WITH YOUR OK MESSAGE")})
	public Response getCheckConnection() {
		String string_response = "connected";

		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "https://bildungsportal.sachsen.de/")
				.entity(string_response)
				.build();
	}

	private final static L2pLogger logger = L2pLogger.getInstance(OnyxInteractionTrackerProxyService.class.getName());
	public OnyxInteractionTrackerProxyService() {
		L2pLogger.setGlobalConsoleLevel(Level.INFO);
	}

	/**
	 * Main functionality function. Receives data in JSON form app,
	 * makes xAPI-Statements from it and forwards them to MobSOS.
	 *
	 * @param dataString The data in JSON form.
	 * @return Returns an HTTP response confirming a successful post to the LRS.
	 */
	@POST
	@Path("/post-inputs")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	@ApiResponses(
			value = {@ApiResponse(
					code = HttpURLConnection.HTTP_OK,
					message = "Json-Text has been posted.")})
	@ApiOperation(
			value = "Post Json",
			notes = "Receives data in JSON-Text form from app and returns JSON-Text")
	public Response postInputs(String dataString) throws URISyntaxException {
		logger.info("Received request.");
		JSONObject properDataJSON = new JSONObject(dataString);
		logger.info(properDataJSON.toString());

		StatementGenerator generator = new StatementGenerator();
		JSONObject statement = generator.createStatementFromAppData(properDataJSON, logger);

		if (statement == null) {
			logger.warning("Format of request data is wrong.");
			String returnString = "{\"msg\": \"Wrong data formulation.\"}";
			return Response
					.status(Response.Status.BAD_REQUEST)
					.header("Access-Control-Allow-Origin", "https://bildungsportal.sachsen.de/")
					.entity(returnString)
					.type(MediaType.APPLICATION_JSON)
					.build();
		} else {
			// Create Tokens-Array
			JSONArray tokens = new JSONArray();
			tokens.put(properDataJSON.getString("userId"));

			// Create EventMessage from statment and tokens-array
			EventMessage eventMessage = new EventMessage();
			String message = eventMessage.createEventMessage(statement, tokens);

			logger.info("Created event message: " + message);
			Context.get().monitorEvent(MonitoringEvent.SERVICE_CUSTOM_MESSAGE_1, message);
			Context.get().monitorEvent(MonitoringEvent.SERVICE_CUSTOM_MESSAGE_2, properDataJSON.toString());
		}

		String returnString = "{\"msg\": \"Statement successfully created.\"}";
		logger.info("Request response is: " + returnString);
		logger.info("Received Json: " + dataString);

		return Response
				.status(Response.Status.OK)
				.header("Access-Control-Allow-Origin", "https://bildungsportal.sachsen.de/")
				.entity(returnString)
				.type(MediaType.APPLICATION_JSON)
				.build();
	}
}

class EventMessage {
	String createEventMessage(JSONObject statement, JSONArray tokens) {
		/**
		 * This method creates a new JSON-Object (as a String) from the statement and the token array with all valid
		 * tokens to create an event message in the correct format which is needed for MobSOS.
		 */
		JSONObject json_eventMessage = new JSONObject();
		json_eventMessage.put("statement", statement);
		json_eventMessage.put("tokens", tokens);
		String eventMessage = json_eventMessage.toString();
		return eventMessage;
	}
}