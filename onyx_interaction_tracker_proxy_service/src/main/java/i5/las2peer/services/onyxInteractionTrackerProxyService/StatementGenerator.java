package i5.las2peer.services.onyxInteractionTrackerProxyService;

import org.json.JSONException;
import org.json.JSONObject;
import i5.las2peer.logging.L2pLogger;


public class StatementGenerator {

    private final String TECH4COMP_URI = "https://tech4comp.de/xapi";
    private final L2pLogger log = L2pLogger.getInstance(StatementGenerator.class.getName());

    public JSONObject createStatementFromAppData(JSONObject dataJSON, L2pLogger logger) {
        logger.info("createStatementFromAppData-Method is called");

        String userId = dataJSON.getString("userId");
        String studyName = dataJSON.getString("studyName");

        JSONObject retStatement = new JSONObject();

        // Add actor
        try {
            JSONObject actorJSON =  createActor(userId);
            retStatement.put("actor", actorJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the actor data");
            return null;
        }

        //Add verb
        try {
            JSONObject verbJSON = createVerb("evaluated");
            retStatement.put("verb", verbJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the verb data");
            return null;
        }

        //Add object
        try {
            JSONObject objectJSON = createObject(studyName);
            retStatement.put("object", objectJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the object data");
            return null;
        }

        //Add context extensions
        try {
            JSONObject contextJSON = createContextExtensions(dataJSON, logger);
            retStatement.put("context", contextJSON);
        } catch (JSONException e) {
            log.severe("There was a problem parsing the context extension data");
            return null;
        }

        return retStatement;
    }

    private JSONObject createActor(String userId) {
        JSONObject actorJSON = new JSONObject();
        actorJSON.put("objectType", "Agent");
        String mboxValue = "mailto:" + userId + "@tech4comp.com";
        actorJSON.put("mbox", mboxValue);

        return actorJSON;
    }

    private JSONObject createVerb(String verb) {
        JSONObject verbJSON = new JSONObject();

        String id = TECH4COMP_URI + "/verbs/" + verb;
        verbJSON.put("id", id);

        JSONObject displayJSON = new JSONObject();
        displayJSON.put("en-US", verb);
        verbJSON.put("display", displayJSON);

        return verbJSON;
    }

    private JSONObject createObject(String studyName) throws JSONException {
        JSONObject objectJSON = new JSONObject();

        String id = TECH4COMP_URI + "/activities/study_" + studyName;
        objectJSON.put("id", id);

        String objectType = "Activity";
        objectJSON.put("objectType", objectType);

        JSONObject definitionJSON = new JSONObject();
        JSONObject nameJSON = new JSONObject();
        nameJSON.put("en-US", "Study_" + studyName);
        JSONObject descriptionJSON = new JSONObject();
        descriptionJSON.put("en-US", "A tech4comp Onyx-Interaction study evaluation");
        definitionJSON.put("name", nameJSON);
        definitionJSON.put("description", descriptionJSON);
        objectJSON.put("definition", definitionJSON);

        return objectJSON;
    }

    private JSONObject createContextExtensions(JSONObject dataJSON, L2pLogger logger) throws JSONException {
        logger.info("createContextExtensions-Method is called");

        String fragebogenTitle = dataJSON.getString("fragebogenTitle");
        logger.info("fragebogenTitle successfully read");
        String taskId = dataJSON.getString("taskId");
        logger.info("taskId successfully read");
        String taskTitle = dataJSON.getString("taskTitle");
        logger.info("taskTitle successfully read");
        String taskTypeIndex = dataJSON.getString("taskTypeIndex");
        logger.info("taskTypeIndex successfully read");
        String taskElementId = dataJSON.getString("taskElementId");
        logger.info("taskElementId successfully read");

        String inputFieldIndex = dataJSON.getString("inputFieldIndex");
        logger.info("inputFieldIndex successfully read");
        String taskInputValue = dataJSON.getString("taskInputValue");
        logger.info("taskInputValue successfully read");

        String inputCounter = dataJSON.getString("inputCounter");
        logger.info("inputCounter successfully read");
        String timestamp = dataJSON.getString("timestamp");
        logger.info("timestamp successfully read");
        String startTimestamp = dataJSON.getString("startTimestamp");
        logger.info("startTimestamp successfully read");
        String jsonVersion = dataJSON.getString("jsonVersion");
        logger.info("jsonVersion successfully read");

        String iri = TECH4COMP_URI + "/context/extensions/";
        JSONObject extensionsJSON = new JSONObject();

        extensionsJSON.put(iri + "fragebogenTitle", fragebogenTitle);
        extensionsJSON.put(iri + "taskId", taskId);
        extensionsJSON.put(iri + "taskTitle", taskTitle);
        extensionsJSON.put(iri + "taskTypeIndex", taskTypeIndex);
        extensionsJSON.put(iri + "taskElementId", taskElementId);
        extensionsJSON.put(iri + "inputFieldIndex", inputFieldIndex);
        extensionsJSON.put(iri + "taskInputValue", taskInputValue);
        extensionsJSON.put(iri + "inputCounter", inputCounter);
        extensionsJSON.put(iri + "timestamp", timestamp);
        extensionsJSON.put(iri + "startTimestamp", startTimestamp);
        extensionsJSON.put(iri + "jsonVersion", jsonVersion);
        logger.info("creating new json successful");

        JSONObject contextJSON = new JSONObject();
        contextJSON.put("extensions", extensionsJSON);
        return contextJSON;
    }
}