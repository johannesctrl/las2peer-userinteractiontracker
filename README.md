![](https://raw.githubusercontent.com/rwth-acis/las2peer/master/img/logo/bitmap/las2peer-logo-128x128.png)

# las2peer-ONYX-Interaction-Tracker

The las2peer-ONYX-Interaction-Tracker is a service to facilitate communication between the ONYX-Interaction-Tracker and a designated Learning Record Store.
* The ONYX-Interaction-Tracker ([project](https://gitlab.com/johannesctrl/onyx-interactions-tracker)) is a JavaScript-Script that can be embedded into a test or single task of the ONYX Testsuite (run in a browser) to track inputs a user does while working on a task. On every change of an input the JavaScript-Script sends a POST-Method to this service.
* The Learning Record Store is a storage to store learning achievements of a person or a group. The used kind of LRS for this project is the [Learning Locker](https://docs.learninglocker.net/welcome/).

As the following image shows this service converts a json formatted user interaction created by the ONYX-User-Interaction-Tracker into an xAPI statement. This xAPI statement can be accepted by the Learning Record Store.

![](show.jpg)

The project is based on the [las2peer-template-project](https://github.com/rwth-acis/las2peer-template-project)
For documentation on the las2peer service API, please refer to the [wiki](https://github.com/rwth-acis/las2peer-Template-Project/wiki).


## REST API calls
The service provides the two following api calls.

|request method|url|description|
|---|---|---|
|GET|/uit-proxy/getCheckConnection|This method simply checks if the service can be reached by sending back a string with value "connected".|
|POST|/uit-proxy/postInputs|This method needs a json formatted single input of a user as a POST-payload to send it via MobSOS to the Learning Record Store. It converts the json object into a xAPI statement which is needed by the LRS.|
